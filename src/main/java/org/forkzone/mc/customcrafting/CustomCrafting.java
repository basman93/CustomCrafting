package org.forkzone.mc.customcrafting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.java.JavaPlugin;


/**
 * CustomCrafting Plugin for Bukkit by basma93
 *
 * <P>Allows to add custom crafting recipes for the workbench or furnace
 *
 * @author basman93
 * @version 0.0.3
 *
 */
public final class CustomCrafting extends JavaPlugin
{
	/** Prefix {@value} for chat messages */
	private static final String PREFIX = "[CustomCrafting] ";

	/** Version number to check if the configfile is uptodate */
	private static final int CONFIG_VERSION = 1;

	/**
	 * @param shape (required) raw shape string
	 * @return returns the raw shape as an array:<br>
	 * first level are the rows of the recipe,<br>
	 * second level are the collums of the recipe,<br>
	 * third level are the item ID's and Data values.
	 */
	private String[][][] getShapeArray(String shape)
	{
		String[][][] shape_array = new String[3][3][2];
		String[] buffer = shape.split(";");

		for(int i = 0; i < buffer.length; ++i)
		{
			String[] buffer_ = buffer[i].split(",");
			for(int j = 0; j < buffer_.length; ++j)
			{
				String[] buffer__ = buffer_[j].split(":");

				shape_array[i][j][0] = buffer__[0].toUpperCase();
				shape_array[i][j][1] = buffer__.length > 1 ? buffer__[1] : "0";
			}
		}

		return shape_array;
	}

	/**
	 * @param shape (required) raw shape string
	 * @return returns the raw shape as an array:<br>
	 * first level are the different items,<br>
	 * second level are the different attribute of the item:<br>
	 * <ol>
	 * <li>Item ID</li>
	 * <li>Item data value</li>
	 * <li>Item amount</li>
	 * </ol>
	 */
	private String[][] getShapeArraySL(String shape)
	{
		ArrayList<String[]> shape_array = new ArrayList<String[]>();

		String[] buffer = shape.split(",");
		for(String buffer_ : buffer)
		{
			shape_array.add(getItemArray(buffer_));
		}

		return shape_array.toArray(new String[shape_array.size()][3]);
	}

	/**
	 * @param item (required) raw item string
	 * @return returns the raw shape as an array:<br>
	 * <ol>
	 * <li>Item ID</li>
	 * <li>Item data value</li>
	 * <li>Item amount</li>
	 * </ol>
	 */
	private String[] getItemArray(String item)
	{
		String[] item_array = new String[3];
		String[] buffer_item = item.split("#");

		String[] buffer_item_ = buffer_item[0].split(":");
		item_array[0] = buffer_item_[0].toUpperCase();
		item_array[1] = buffer_item_.length > 1 ? buffer_item_[1] : "0";
		item_array[2] = buffer_item.length > 1 ? buffer_item[1] : "1";

		return item_array;
	}

	/** parse a string to a int */
	private int toInt(String string)
	{
		return Integer.parseInt(string);
	}

	/** parse a string to a short */
	private short toShort(String string)
	{
		return Short.parseShort(string);
	}

	/**
	 * Adds a custom furnace recipe to bukkit
	 * @param name (required) the name of the recipe
	 * @param shape (required) the raw shape of the input item
	 * @param item (required) the raw shape of the output item
	 * @param xp (required) the xp the player will get for this
	 */
	@SuppressWarnings("deprecation")
	private void addNewFurnaceRecipe(String name, String shape, String item, float xp, String nbtdata)
	{
		String[] item_array = getItemArray(item);
		if(Material.getMaterial(item_array[0]) == null)
		{
			getLogger().warning("Could not add \"" + name + "\"");
			getLogger().warning("Unknown: " + item_array[0]);
			return;
		}

		String[] shape_array = getItemArray(shape);
		if(Material.getMaterial(shape_array[0]) == null)
		{
			getLogger().warning("Could not add \"" + name + "\"");
			getLogger().warning("Unknown: " + shape_array[0]);
			return;
		}

		ItemStack is = new ItemStack(Material.getMaterial(item_array[0]), toInt(item_array[2]), toShort(item_array[1]));
		if(nbtdata != null && !nbtdata.isEmpty())
			Bukkit.getUnsafe().modifyItemStack(is, nbtdata);
		FurnaceRecipe recipe = new FurnaceRecipe(is, new MaterialData(Material.getMaterial(shape_array[0]), (byte) 0), xp);
		if(!Bukkit.getServer().addRecipe(recipe))
			getLogger().warning("Something went wrong! Cannot add \"" + name + "\"");
		else
			getLogger().info("Add Furnace Recipe \"" + name + "\"");
	}

	/**
	 * Adds a custom crafting recipe to bukkit
	 * @param name (required) the name of the recipe
	 * @param shape (required) the raw shape of the input item
	 * @param item (required) the raw shape of the output item
	 * @param shapeless (required) if the recipe is shapeless
	 */
	@SuppressWarnings("deprecation")
	private void addNewRecipe(NamespacedKey name, String shape, String item, boolean shapeless, String nbtdata)
	{
		String[] item_array = getItemArray(item);
		if(Material.getMaterial(item_array[0]) == null)
		{
			getLogger().warning("Could not add \"" + name.getKey() + "\"");
			getLogger().warning("Unknown: " + item_array[0]);
			return;
		}
		else if(shapeless)
		{
			String[][] shape_array = getShapeArraySL(shape);
			ItemStack is = new ItemStack(Material.getMaterial(item_array[0]), toInt(item_array[2]), toShort(item_array[1]));
			if(nbtdata != null && !nbtdata.isEmpty())
				Bukkit.getUnsafe().modifyItemStack(is, nbtdata);
			ShapelessRecipe recipe = new ShapelessRecipe(name, is);

			for(String[] ingredient_array : shape_array)
			{
				recipe.addIngredient(toInt(ingredient_array[2]), Material.getMaterial(ingredient_array[0]), toInt(ingredient_array[1]));
			}

			if(!Bukkit.getServer().addRecipe(recipe))
				getLogger().warning("Something went wrong! Cannot add \"" + name.getKey() + "\"");
			else
				getLogger().info("Add Recipe \"" + name.getKey() + "\"");
		}
		else
		{
			String[][][] shape_array = getShapeArray(shape);
			ItemStack is = new ItemStack(Material.getMaterial(item_array[0]), toInt(item_array[2]), toShort(item_array[1]));
			if(nbtdata != null && !nbtdata.isEmpty())
				Bukkit.getUnsafe().modifyItemStack(is, nbtdata);
			ShapedRecipe recipe = new ShapedRecipe(name, is);
			Map<String, Character> map = new HashMap<String, Character>();
			int counter = 0;
			String[] recipe_shape = new String[3];
			for(int i = 0; i < shape_array.length; ++i)
			{
				recipe_shape[i] = "";
				for(int j = 0; j < shape_array[i].length; ++j)
				{
					if(!map.containsKey(shape_array[i][j][0] + ":" + shape_array[i][j][1]))
					{
						if(shape_array[i][j][0].equalsIgnoreCase(" ") || shape_array[i][j][0].equalsIgnoreCase("0") || shape_array[i][j][0].equalsIgnoreCase("air"))
							map.put(shape_array[i][j][0] + ":" + shape_array[i][j][1], ' ');
						else
						{
							map.put(shape_array[i][j][0] + ":" + shape_array[i][j][1], (char)('a' + counter));
							++counter;
						}
					}
					recipe_shape[i] += map.get(shape_array[i][j][0] + ":" + shape_array[i][j][1]);
				}
			}
			recipe.shape(recipe_shape);
			for (Map.Entry<String, Character> entry : map.entrySet())
			{
				if(!entry.getValue().equals(' '))
				{
					String[] buffer = entry.getKey().split(":");
					recipe.setIngredient(entry.getValue(), Material.getMaterial(buffer[0]), toInt(buffer[1]));
				}
			}
			if(!Bukkit.getServer().addRecipe(recipe))
				getLogger().warning("Something went wrong! Cannot add \"" + name.getKey() + "\"");
			else
				getLogger().info("Add Recipe \"" + name.getKey() + "\"");
		}
	}

	/**
	 * reads the configfile and adds every recipe found in it.
	 */
	private void readConfigAndAddRecipe()
	{
		for(String key : getConfig().getConfigurationSection("recipes").getKeys(false))
		{
			NamespacedKey nkey = new NamespacedKey(this, key);
			addNewRecipe(
					nkey,
					getConfig().getString("recipes." + key + ".shape"),
					getConfig().getString("recipes." + key + ".item"),
					getConfig().getBoolean("recipes." + key + ".shapeless"),
					getConfig().getString("recipes." + key + ".nbt")
			);
		}

		for(String key : getConfig().getConfigurationSection("furnace_recipes").getKeys(false))
		{
			addNewFurnaceRecipe(
					key,
					getConfig().getString("furnace_recipes." + key + ".shape"),
					getConfig().getString("furnace_recipes." + key + ".item"),
					(float) getConfig().getDouble("furnace_recipes." + key + ".xp"),
					getConfig().getString("furnace_recipes." + key + ".nbt")
			);
		}
	}

	/**
	 * removes the custom recipes from bukkit.
	 * At this moment its only possible to reset the recipes list to default.
	 */
	private void removeRecipe()
	{
		Bukkit.getServer().resetRecipes();
	}

	@Override
	public void onEnable()
	{
		saveDefaultConfig();
		reloadConfig();

		// Checks if the configfile is the required version, give out a warning otherwise.
		if(getConfig().getInt("version", 0) < CONFIG_VERSION)
			getLogger().warning("Please delete your configfile. Your configfile is outdated!");

		readConfigAndAddRecipe();
	}

	@Override
	public void onDisable()
	{
		removeRecipe();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(cmd.getName().equalsIgnoreCase("customcrafting"))
		{
			if(args.length == 1 && args[0].equalsIgnoreCase("reload") && sender.hasPermission("customcrafting.reload"))
			{
				removeRecipe();
				saveDefaultConfig();
				reloadConfig();
				readConfigAndAddRecipe();
				sender.sendMessage(PREFIX + "reloaded!");
			}
			else if(sender.hasPermission("customcrafting.infolite") || sender.hasPermission("customcrafting.info"))
			{
				sender.sendMessage(PREFIX + "Plugin Information");
				sender.sendMessage(PREFIX + "Name: " + getDescription().getName());
				sender.sendMessage(PREFIX + "Author: " + String.join(", ", getDescription().getAuthors()));
				if(sender.hasPermission("customcrafting.info"))
				{
					sender.sendMessage(PREFIX + "Version: " + getDescription().getVersion());
					sender.sendMessage(PREFIX + "Arguments: reload");
				}
			}
			return true;
		}
		return false;
	}
}
